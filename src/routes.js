import React from 'react';
import { Router, IndexRoute, Route } from 'react-router';

import App from './app';
import ProductList from './popular';
import Home from './home';
import Post from './post';
import Canvas from './canvas';
import Form from './form';

const Routes = (props) => (
  <Router {...props}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="/p/:page" component={Home} />
      <Route path="/post/:slug" component={Post} />
      <Route path="/popular" component={ProductList} />
      <Route path="/canvas" component={Canvas} />
      <Route path="/form" component={Form} />
    </Route>
  </Router>
);

export default Routes;