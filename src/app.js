import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Alistairs Javascript Universe</h2>
          <a href="/">Home</a>
          <a href="/popular">Popular</a>
          <a href="/canvas">Canvas</a>
          <a href="/form">Form</a>
        </div>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;