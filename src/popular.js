import React from 'react';
// import ReactDOM from 'react-dom';
import './style.css';
// import aqua from './images/products/image-aqua.png';
// import rose from './images/products/image-rose.png';
// import steel from './images/products/image-steel.png';
// import yellow from './images/products/image-yellow.png';
// import avatar1 from './images/avatars/daniel.jpg';
// import avatar2 from './images/avatars/kristy.png';
// import avatar3 from './images/avatars/veronika.jpg';
// import avatar4 from './images/avatars/molly.png';
// import * as Seed from './seed.js';
import aqua from './images/products/image-aqua.png';
import rose from './images/products/image-rose.png';
import steel from './images/products/image-steel.png';
import yellow from './images/products/image-yellow.png';
import avatar1 from './images/avatars/daniel.jpg';
import avatar2 from './images/avatars/kristy.png';
import avatar3 from './images/avatars/veronika.jpg';
import avatar4 from './images/avatars/molly.png';


      var productsList = [
      {
        id: 1,
        title: 'Yellow Pail',
        description: 'On-demand sand castle construction expertise.',
        url: '#',
        votes: generateVoteCount(),
        submitterAvatarUrl: {avatar1},
        productImageUrl: {aqua},
      },
      {
        id: 2,
        title: 'Supermajority: The Fantasy Congress League',
        description: 'Earn points when your favorite politicians pass legislation.',
        url: '#',
        votes: generateVoteCount(),
        submitterAvatarUrl: {avatar2},
        productImageUrl: {rose},
      },
      {
        id: 3,
        title: 'Tinfoild: Tailored tinfoil hats',
        description: 'We already have your measurements and shipping address.',
        url: '#',
        votes: generateVoteCount(),
        submitterAvatarUrl: {avatar3},
        productImageUrl: {steel},
      },
      {
        id: 4,
        title: 'Haught or Naught',
        description: 'High-minded or absent-minded? You decide.',
        url: '#',
        votes: generateVoteCount(),
        submitterAvatarUrl: {avatar4},
        productImageUrl: {yellow},
      },
    ];

class ProductList extends React.Component {

    state = {
        products: [],
      };

      componentDidMount() {
        this.setState({ products: productsList });
      }

      handleProductUpVote = (productId) => {
        const nextProducts = this.state.products.map((product) => {
          if (product.id === productId) {
            return Object.assign({}, product, {
              votes: product.votes + 1,
            });
          } else {
            return product;
          }
        });
        this.setState({
          products: nextProducts,
        });
      }

    render() {

        const products = this.state.products.sort((a, b) => (
            b.votes - a.votes
        ));

        const productComponents = products.map((product) => (
            <Product
              key={'product-' + product.id}
              id={product.id}
              title={product.title}
              description={product.description}
              url={product.url}
              votes={product.votes}
              submitterAvatarUrl={product.submitterAvatarUrl}
              productImageUrl={product.productImageUrl}
              onVote={this.handleProductUpVote}
            />
          ));

        return (
            <div className="container">
                <h1>Popular products</h1>
                <hr />
                {productComponents}
            </div>
        );
    }
}

class Product extends React.Component {

    constructor() {
        super();
        this.passTheId = this.passTheId.bind(this);
    }

    passTheId() {
        console.log('Id will be passed');
        this.props.onVote(this.props.id);
    }


    render() {
        return (
          <div className='container'>
            <div className="row">
            <div className='col-md-12'>


                <div className="main">
                <div className="image">  
                    <img src={this.props.productImageUrl} />
                </div> 

                <div className='header'>
                    <a onClick={this.passTheId}>
                        <i className='fa fa-2x fa-caret-up' />
                    </a>
                    {this.props.votes}
                </div>
                <div className='description'>
                    <a href={this.props.url}>
                        {this.props.title}
                    </a>
                    <p>
                        {this.props.description}
                    </p>
                </div>
              <div className='extra'>
                <span>Submitted by:</span>
                <img
                  className='avatar'
                  src={this.props.submitterAvatarUrl}
                />
              </div>
              </div>


            </div>
            </div>
          </div>
        );
      }
}

export default ProductList;

// ReactDOM.render(<ProductList />, document.getElementById('content'));
// ========================================

// ReactDOM.render(<ProductList />, document.getElementById('root'));

    function generateVoteCount() {
      return Math.floor((Math.random() * 50) + 15);
    }


// ReactDOM.render(
//   <Game />,
//   document.getElementById('root')
// );

  