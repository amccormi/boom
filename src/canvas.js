import React from 'react';
import ocean from './images/ocean.jpg';

class Canvas extends React.Component {
  componentDidMount() {
    const canvas = this.refs.canvas
    const ctx = canvas.getContext("2d")
    const img = this.refs.image
    img.onload = () => {
      ctx.drawImage(img, 0, 0)
      ctx.font = "40px Courier"
      ctx.fillText(this.props.text, 210, 75)
    }
  }
  render() {
    return(
      <div>
        <canvas ref="canvas" width={940} height={425} />
        <img id="helloimage" ref="image" src={ocean} className="hidden" />
      </div>
    )
  }
}
export default Canvas;